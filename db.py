import sqlite3

DB_NAME = 'messages.db'
TABLE_NAME = 'messages'

class DB:
    def __init__(self):
        self.connection = self.connect()
        self.create_table()

    def create_table(self):
        '''Создаем таблицу для сохранения сообщений'''
        cursor = self.connection.cursor()
        cursor.execute(f'CREATE TABLE IF NOT EXISTS {TABLE_NAME}(copy_message_id BIGINT, new_message_id BIGINT)')
        self.connection.commit()
        
    def connect(self):
        '''Подключаемся к базе данных'''
        try:
            connection = sqlite3.connect(DB_NAME)
            return connection
        except Exception as ex:
            print('Ошибка при подключении к базе данных', ex, sep=' ')

    def add_message(self, copy_message_id, new_message_id):
        '''Добавляем скопированные ид сообщений в базу данных'''
        cursor = self.connection.cursor()
        cursor.execute(f'INSERT INTO {TABLE_NAME}(copy_message_id, new_message_id) VALUES(?, ?)', (copy_message_id, new_message_id,))
        self.connection.commit()

    def get_message(self, copy_message_id):
        '''Получаем сообщение из базы данных по скопированному id'''
        try:
            cursor = self.connection.cursor()
            cursor.execute(f'SELECT * FROM {TABLE_NAME} WHERE copy_message_id = ? ', (copy_message_id,))

            return cursor.fetchall()[0]
        except IndexError:
            return None

db = DB()