import asyncio
import re
from log import log
from config import config
from telethon import TelegramClient, events
from helpers import silentremove, filter_text
from db import db

class BOT:
    def __init__(self):
        self.channel_copy_id = int(config['bot']['channel_copy_id'])
        self.channel_new_id = int(config['bot']['channel_new_id'])
        
        self.client = TelegramClient('bot', config['bot']['api_id'], config['bot']['api_hash'])
        self.client.add_event_handler(self.on_message, events.NewMessage)

    async def check_channels_for_exists(self):
        '''Проверка указанных ид каналов из конфига'''

        log.info('Поиск необходимых каналов...')
        is_channel_copy = False
        is_channel_new = False

        async for dialog in self.client.iter_dialogs():
            if self.channel_copy_id == dialog.id:
                is_channel_copy = True
            
            if self.channel_new_id == dialog.id:
                is_channel_new = True
        
        if not is_channel_copy:
            raise Exception('Не найден канал-донор (channel_copy_id) в чатах')
        
        if not is_channel_new:
            raise Exception('Не найден канал для переотправки (channel_new_id) в чатах')

    async def on_message(self, event):
        '''Обработчик на новое сообщение от канала'''
        try:
            await asyncio.sleep(2)

            # Если сообщение не от нужного канала, выходим
            if self.channel_copy_id != event.chat_id:
                return

            message_id = event.id
            reply_message_id = event.reply_to_msg_id
            message_media = event.media
            message_text = filter_text(event.text)

            reply_to_in_new_channel = None
            if reply_message_id:
                exists_message = db.get_message(reply_message_id) 
                if exists_message:
                    reply_to_in_new_channel = exists_message[1]

            if message_text: 
                # Если сообщение не пустое, добавляем к нему название проекта
                message_text = '{msg_text}\n\n{channel_title}'.format(
                    msg_text=message_text, 
                    channel_title=config['bot']['channel_new_title']
                )

            if message_media:
                log.info('Получили медиа с канала-донора')
                response = await self.send_received_file(event, message_text, reply_to_in_new_channel)
            else:
                log.info('Получили сообщение с канала-донора')
                if message_text:
                    response = await self.client.send_message(
                        self.channel_new_id, 
                        message_text, 
                        link_preview=False,
                        reply_to=reply_to_in_new_channel
                    )

            db.add_message(message_id, response.id)

        except Exception as ex:
            log.error(f'Ошибка в получении сообщения {ex}')

    async def send_received_file(self, event, message_text = '', reply_to=None):
        '''Переотправлка полученого медиа файла в новый канал'''
        try:
            path = await event.download_media() #Скачиваем получение медиа
            response = await self.client.send_file(
                self.channel_new_id, 
                path, 
                caption = message_text,
                reply_to=reply_to
            ) # Отправляем медиа

            silentremove(path) #Удаляем скачаный файл

            return response
        except Exception as ex:
            log.error(f'Ошибка при отправке медиа файла {ex}')

    def start(self):
        '''Запуск всего парада'''
        with self.client:
            self.client.loop.run_until_complete(self.check_channels_for_exists())
            log.info('Запуск бота...')
            self.client.start(config['bot']['phone_number']) 
            self.client.run_until_disconnected()

        #self.client.loop.run_until_complete(self.main_loop())

    async def main_loop(self, interval=1):
        try:
            await self.client.connect()
            log.info('Бот запущен!')
        except Exception as e:
            log.error('Ошибка при подключении к боту {}'.format(ex))
            return

        try:
            connect_attempts = 1
            while True:
                if not self.client.is_connected():
                    connect_attempts += 1
                    log.warning(f'Нету соединение, пытаемся подключиться ... № {connect_attempts}')
                    await self.client.connect()

                await asyncio.sleep(interval)

        except KeyboardInterrupt:
            pass
        finally:
            await self.client.disconnect()

def start_bot():
    bot = BOT()
    bot.start()
