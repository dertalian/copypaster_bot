import configparser
import os
import log
from helpers import is_integer

CONFIG_PATH = 'config.ini'
config = configparser.ConfigParser()

def config_init():
    # Если файл не найден или файл пустой, инициализируем его
    if not os.path.isfile(CONFIG_PATH) or os.path.getsize(CONFIG_PATH) == 0:
        config.add_section('bot')
        config['bot']['api_id'] = ''
        config['bot']['api_hash'] = ''
        config['bot']['phone_number'] = ''
        config['bot']['channel_copy_id'] = ''
        config['bot']['channel_new_id'] = ''
        config['bot']['channel_new_title'] = ''

        with open(CONFIG_PATH, 'w') as configfile:
            config.write(configfile)
    else:
        config.read(CONFIG_PATH)

    if not config['bot']['api_id'] or not config['bot']['api_hash']:
        raise Exception('API_HASH и API_ID бота не могут быть пустым!')

    if not config['bot']['phone_number']:
        raise Exception('номер телефона (phone_number) не может быть пустым!')

    if not config['bot']['channel_copy_id']:
        raise Exception('ID канала-донора (channel_copy_id) не может быть пустым!')

    if not config['bot']['channel_new_id']:
        raise Exception('ID нового канала для получения постов (channel_new_id) не может быть пустым!')

    if not is_integer(config['bot']['channel_copy_id']):
        raise Exception('ID канал-донора (channel_copy_id) должен быть числом!')
    
    if not is_integer(config['bot']['channel_new_id']):
        raise Exception('ID нового канала (channel_new_id) должен быть числом!')