from log import log
from config import config_init
from bot import start_bot 

def main():
    config_init()
    start_bot()

if __name__ == '__main__':
    try:
        log.info('Инициализация...')
        main()
    except Exception as ex:
        log.error('ОШИБКА | {ex}'.format(ex=ex))