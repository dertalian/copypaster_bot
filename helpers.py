import os, errno
import re

def silentremove(filename):
    '''Удаляет существующий файл'''
    try:
        os.remove(filename)
    except OSError as e: 
        if e.errno != errno.ENOENT: 
            raise

def filter_text(text = ''):
    '''Фильтрует полученый текст, удаляет теги канала донора и любой username'''
    donor_title_sep = 'FatWallet'

    text = re.sub(f'{donor_title_sep}.*$', '', text, flags = re.IGNORECASE | re.MULTILINE) # Убираем все заголовки канала-донора
    text = re.sub('@[^\s]+', '', text) # Убираем все юзернеймы в сообщении
    text = text.strip() # Обрезаем ненужные "\n" вокруг текста

    return text

def is_integer(n):
    '''Проверка типа на число'''
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()